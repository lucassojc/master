<?php
    namespace App\Controllers;

    use App\Models\ProductModel;
    use App\Models\CategoryModel;

    class AdminProductMenagementController extends \App\Core\Role\AdminRoleController {
        public function products() {

            $productModel = new ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);
        }

        public function getEdit($productId) {
            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if (!$product) {
                $this->redirect(\Configuration::BASE . 'admin/products');
            }

            $this->set('product', $product);

            return $productModel;
            
        }

        public function postEdit($productId) {
            $productModel = $this->getEdit($productId);

            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);

            $productModel->editById($productId, [
                'title' => $title,
                'description' => $description,
                'price' => $price
            ]);
            
            if (isset($_FILES['image']) && $_FILES['image']['error'] == 0) {
                $uploadStatus = $this->doImageUpload('image', $productId, '.png');
                if (!$uploadStatus) {
                    $this->set('message', 'Proizvod je izmenjen, ali nije dodata nova slika.');
                    return;
                } 
            }    

            $this->redirect(\Configuration::BASE . 'admin/products');
        }

        public function getAdd() {
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function postAdd() {
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
            $administratorId =  $this->getSession()->get('administrator_id');
            $categoryId = \filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_STRING);

            $productModel = new ProductModel($this->getDatabaseConnection());

            $productId = $productModel->add([
                'title' => $title,
                'description' => $description,
                'price' => $price,
                'category_id' => $categoryId,
                'administrator_id' => $administratorId
            ]);

            $uploadStatus = $this->doImageUpload('image', $productId, '.png');
            if (!$uploadStatus) {
                return;
            }

            if ($productId) {
                $this->redirect(\Configuration::BASE . 'admin/products');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovaj proizvod.');
        }

        private function doImageUpload (string $fieldName, string $productId): bool {
            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById(intval($productId));
            # codeguy/upload

            unlink(\Configuration::UPLOAD_DIR . $product->image);

            $storage = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $storage);
            $file->setName($productId);
            $file->addValidations([
                new \Upload\Validation\Mimetype("image/png", "image/jpeg"),
                new \Upload\Validation\Size("3M")
            ]);

            try {
                $file->upload();

                $fullFilename = $file->getNameWithExtension();

                
                $productModel->editById(intval($productId), [
                    'image' => \Configuration::UPLOAD_DIR . $fullFilename
                ]);

                $this->doResize(
                    \Configuration::UPLOAD_DIR . $productId . '.png',
                    \Configuration::DEFAULT_IMAGE_WIDTH,
                    \Configuration::DEFAULT_IMAGE_HEIGHT);

                return true;
            } catch (\Exception $e) {
                $this->set('message', 'Greska: ' . implode(', ', $file->getErrors()));
                return false;
            }
        }

        private function doResize(string $filePath, int $w, int $h) {
            $longer = max($w, $h);
            
            $image = new \Gumlet\ImageResize($filePath);
            $image->resizeToBestFit($longer, $longer);
            $image->crop($w, $h);
            $image->save($filePath);
        }
    }