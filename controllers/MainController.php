<?php
    namespace App\Controllers;

    use App\Models\CategoryModel;
    use App\Models\ProductViewModel;
    use App\Core\Controller;

    class MainController extends Controller {
        public function home() {
            
        }

        public function getLogin() {

        }

        public function postLogin() {
            $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(4)
                ->setMaxLength(120)
                ->isValid($password);
            
            if (!$validanPassword) {
                $this->set('message', 'Došlo je do greške, lozinka nije validna.');
                return;
            }

            $administratorModel = new \App\Models\AdministratorModel($this->getDatabaseConnection());

            $admin = $administratorModel->getByFieldName('username', $username);
            if(!$admin) {
                $this->set('message', 'Došlo je do greške: Ne postoji korisnik sa tim korisničkim imenom.');
                return;
            }

            $isPassword = $administratorModel->getByFieldName('password', $password);
            if (!$isPassword) {
                sleep(1);
                $this->set('message', 'Došlo je do greške: Lozinka nije ispravna.');
                return;
            }

            $this->getSession()->put('administrator_id', $admin->administrator_id);
            $this->getSession()->save();
            $this->redirect(\Configuration::BASE . 'admin-panel');


        }

        public function getLogout() {
            $this->getSession()->remove('administrator_id');
            $this->getSession()->save();
            $this->redirect(\Configuration::BASE);
        }

        public function showContact() {
            
        }
    }