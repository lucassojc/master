<?php
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\OrderModel;
    use App\Models\CartModel;

    class OrderController extends Controller {
        public function getAdd() {
        }

        public function postAdd() {
            $customerFirstName = \filter_input(INPUT_POST, 'customer_first_name', FILTER_SANITIZE_STRING);
            $customerLastName = \filter_input(INPUT_POST, 'customer_last_name', FILTER_SANITIZE_STRING);
            $customerEmail = \filter_input(INPUT_POST, 'customer_email', FILTER_SANITIZE_STRING);
            $customerAddress = \filter_input(INPUT_POST, 'customer_address', FILTER_SANITIZE_STRING);

            $cartModel = new CartModel($this->getDatabaseConnection());

            $sessionNumber = $this->getSession()->get('__fingerprint');
            $cartIdObject = $cartModel->getBySessionNumber('session_number', $sessionNumber);
            $cartId = $cartIdObject->cart_id;

            $orderModel = new OrderModel($this->getDatabaseConnection());

            $orderId = $orderModel->add([
                'customer_first_name' => $customerFirstName,
                'customer_last_name' => $customerLastName,
                'customer_email' => $customerEmail,
                'customer_address' => $customerAddress,
                'order_status' => 'pending',
                'cart_id' => $cartId
            ]);

            if (!$orderId) {
                print_r($cartId);
                $this->set('message', 'Došlo je do greške: Podaci nisu ispravni.');
            } 
        }

        private function notifyUser($items) {
            $items = $this->getSession()->get('items', []);
            $this->set('items', $items);

            $orderModel = new OrderModel($this->getDatabaseConnection());
            $order = $orderModel->getByFieldName('cart_id', $orderId);

            $html = '<!doctype html><html><head><meta charset="utf-8"></head><body>';
            $html .= 'Spisak kupljenih proizvoda: ';
            $html .= \htmlspecialchars(json_encode($items));
            $html .= '</body></html>';

            $event = new EmailEventHandler();
            $event->setSubject('Lista kupljenih proizvoda');
            $event->setBody($html);
            $event->addAddress($order->customer_email);

            $eventModel = new EventModel($this->getDatabaseConnection());
            $eventModel->add ([
                'type' => 'email',
                'data' => $event->getData()
            ]);
 
        }
    }