<?php
    namespace App\Controllers;

    use App\Core\ApiController;
    use App\Models\ProductModel;
    use App\Models\CartProductModel;


    class ApiCartController extends ApiController {
        public function getItems() {
            $items = $this->getSession()->get('items', []);
            $this->set('items', $items);
        }

        public function addCart($productId, $quantity) {

            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if(!$product) {
                $this->set('error', -1);
                return;
            }

            $items = $this->getSession()->get('items', []);

            foreach ($items as $cart) {
                if ($cart->product_id == $productId) {
                    $this->set('error', -2);
                    return;
                }
            }

			$product->quantity = $quantity;
            $items[] = $product;
            $this->getSession()->put('items', $items);

            $this->set('error', 0);
            return;
        }

        public function clear() {
            $this->getSession()->put('items', []);
            
            $this->set('error', 0);
        }

        public function delete($productId) {
            $cartProductModel = new CartProductModel($this->getDatabaseConnection());
            $cartProduct = $cartProductModel->getById($productId);

            if(!$cartProduct) {
                $this->set('error', -1);
                return;
            }

            $items = $this->getSession()->get('items', []);

            foreach ($items as $cart) {
                if ($cart->product_id == $productId) {
                    $this->set('error', -2);
                    return;
                }
            }

            unset($items[$cartProduct]);
            return;
        }
    }