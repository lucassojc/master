<?php
    namespace App\Controllers;

    use App\Models\ProductModel;
    use App\Models\ProductViewModel;
    use App\Core\Controller;

    class ProductController extends Controller {
        public function show($id) {
            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($id);

            if (!$product) {
                header('Location: {{BASE}}');
                exit;
            }

            $this->set('product', $product);

            $productViewModel = new productViewModel($this->getDatabaseConnection());

            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');

            $productViewModel->add(
                [
                    'product_id' => $id,
                    'ip_address' => $ipAddress,
                    'user_agent' => $userAgent
                ]    
            );
        }

        private function normalizeKeywords(string $keywords): string {
            $keywords = trim($keywords);
            $keywords = preg_replace('/ +/', ' ', $keywords);

            return $keywords;
        }

        public function postSearch() {
            $productModel = new ProductModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = $this->normalizeKeywords($q);

            $products = $productModel->getAllBySearch($q);

            $this->set('products', $products);
        }
    }