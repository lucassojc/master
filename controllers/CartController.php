<?php
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\CartModel;
    use App\Models\ProductModel;
    use App\Models\CartProductModel;
    use App\Core\Fingerprint\FingerprintProvider;


    class CartController extends Controller {
        public function getItems() {

            
            $items = $this->getSession()->get('items', []);
            $this->set('items', $items);

            $cartModel = new CartModel($this->getDatabaseConnection());

            $sessionNumber = $this->getSession()->get('__fingerprint');

            $cartModel->add(
                [
                    'session_number' => $sessionNumber
                ]    
            );


            $cartIdObject = $cartModel->getBySessionNumber('session_number', $sessionNumber);
            $cartId = $cartIdObject->cart_id;

            $cartProductModel = new CartProductModel($this->getDatabaseConnection());

            $productModel = new ProductModel($this->getDatabaseConnection());

            $productIds = array_column($items, 'product_id');

            foreach ($productIds as &$productId) {
                
                $cartProductModel->add(
                    [
                        'amount'     => 1,
                        'cart_id'    => $cartId,
                        'product_id' => $productId
                    ]
                );
            }
        }
    }