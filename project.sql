/*
 Navicat Premium Data Transfer

 Source Server         : project
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : project

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 17/12/2018 14:03:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator`  (
  `administrator_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `username` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_active` tinyblob NOT NULL,
  PRIMARY KEY (`administrator_id`) USING BTREE,
  UNIQUE INDEX `uq_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES (1, '0000-00-00 00:00:00', 'admin', 'admin', 0x31);
INSERT INTO `administrator` VALUES (2, '2018-04-15 22:00:38', 'admin1', 'admin1', 0x31);

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `cart_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `session_number` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`cart_id`) USING BTREE,
  UNIQUE INDEX `uq_session_number`(`session_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cart_product
-- ----------------------------
DROP TABLE IF EXISTS `cart_product`;
CREATE TABLE `cart_product`  (
  `cart_product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `added_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `amount` int(10) NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`cart_product_id`) USING BTREE,
  INDEX `fk_cart_product_product_id`(`product_id`) USING BTREE,
  INDEX `fk_cart_product_cart_id`(`cart_id`) USING BTREE,
  CONSTRAINT `fk_cart_product_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cart_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `administrator_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  INDEX `fk_category_administrator_id`(`administrator_id`) USING BTREE,
  CONSTRAINT `fk_category_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`administrator_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin2 COLLATE = latin2_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '0000-00-00 00:00:00', 'Televizori', 'Ovo su televizori.', 'assets/img/c-televizori.png', 1);
INSERT INTO `category` VALUES (2, '0000-00-00 00:00:00', 'Mobilni telefoni', 'Ovo su mobilni telefoni', 'assets/img/c-telefoni.png', 1);
INSERT INTO `category` VALUES (3, '2018-04-17 13:22:39', 'Kompijuteri', 'Ovo su kompijuteri', 'assets/img/c-kompijuteri.png', 1);
INSERT INTO `category` VALUES (4, '2018-05-12 14:54:30', 'Foto oprema', 'Foto aparati i kamere', 'assets/img/c-foto.png', 2);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `customer_first_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `customer_last_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `customer_email` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `customer_address` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `order_status` enum('pending','done') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `fk_order_cart_id`(`cart_id`) USING BTREE,
  CONSTRAINT `fk_order_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `title` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `administrator_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  UNIQUE INDEX `uk_image`(`image`) USING BTREE,
  INDEX `fk_product_administrator_id`(`administrator_id`) USING BTREE,
  INDEX `fk_product_category_id`(`category_id`) USING BTREE,
  CONSTRAINT `fk_product_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`administrator_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '2018-04-15 21:42:17', 'telefon1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-telefon1.png', 10000.00, 1, 2);
INSERT INTO `product` VALUES (2, '0000-00-00 00:00:00', 'telefon2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-telefon2.png', 20000.00, 1, 2);
INSERT INTO `product` VALUES (3, '0000-00-00 00:00:00', 'televizor1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-televizor1.png', 50000.00, 1, 1);
INSERT INTO `product` VALUES (4, '0000-00-00 00:00:00', 'televizor2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-televizor2.png', 100000.00, 1, 1);
INSERT INTO `product` VALUES (5, '2018-05-12 14:55:09', 'kompijuter1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-kompijuter1.png', 35000.00, 2, 3);
INSERT INTO `product` VALUES (7, '2018-05-12 14:56:11', 'kompijuter2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-kompijuter2.png', 60000.00, 2, 3);
INSERT INTO `product` VALUES (8, '2018-05-12 14:56:41', 'Fotoaparat', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-fotoaparat1.png', 50000.00, 1, 4);
INSERT INTO `product` VALUES (9, '2018-05-12 14:57:29', 'Kamera', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non dapibus ex. Vivamus et hendrerit dui. Maecenas et nulla blandit, iaculis dui sit amet, commodo lacus. Vestibulum consectetur sapien non blandit malesuada. Sed scelerisque est at tempus feugiat. Donec arcu arcu, varius sed imperdiet sed, accumsan eu sapien. Fusce eu nibh lacinia tellus condimentum suscipit vel non tellus. Aenean non pretium turpis. Nunc vestibulum, tellus at semper mattis, dolor massa convallis dui, eu dapibus dolor felis quis dolor. Sed eu efficitur orci. Sed rutrum lorem eu odio ultrices dignissim. Proin sollicitudin tellus ac dolor consequat volutpat ac eu est. Aliquam vel feugiat tellus, id efficitur mauris. Quisque vitae justo ante. Aliquam ut pretium libero. Suspendisse potenti.', 'assets/img/p-kamera1.png', 70000.00, 2, 4);

-- ----------------------------
-- Table structure for product_view
-- ----------------------------
DROP TABLE IF EXISTS `product_view`;
CREATE TABLE `product_view`  (
  `product_view_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `product_id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`product_view_id`) USING BTREE,
  INDEX `fk_product_view_product_id`(`product_id`) USING BTREE,
  INDEX `auction_view_ip_address_idx`(`ip_address`) USING BTREE,
  CONSTRAINT `fk_product_view_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
