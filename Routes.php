<?php
    return [
        \App\Core\Route::get('|^categories/?$|',                   'Category',               'getAll'),
        \App\Core\Route::get('|^cart/?$|',                         'Cart',                   'getItems'),
        \App\Core\Route::get('|^checkout/?$|',                     'Order',                  'getAdd'),
        \App\Core\Route::post('|^checkout/?$|',                    'Order',                  'postAdd'),

        \App\Core\Route::get('|^category/([0-9]+)/?$|',            'Category',               'show'),
        \App\Core\Route::get('|^category/([0-9]+)/delete/?$|',     'Category',               'delete'),

        \App\Core\Route::get('|^product/([0-9]+)/?$|',             'Product',                'show'),
        \App\Core\Route::post('|^search/?$|',                      'Product',                'postSearch'),
        \App\Core\Route::get('|^contact/?$|',                      'Main',                   'showContact'),

        #API rute:
        \App\Core\Route::get('|^api/product/([0-9]+)/?$|',         'ApiProduct',             'show'),
        \App\Core\Route::get('|^api/items/?$|',                    'ApiCart',                'getItems'),
        \App\Core\Route::get('|^api/items/add/([0-9]+)/([0-9]+)/?$|',       'ApiCart',       'addCart'),
        \App\Core\Route::get('|^api/items/clear/?$|',              'ApiCart',                'clear'),
        \App\Core\Route::get('|^api/items/delete/([0-9]+)/?$|',    'ApiCart',                'delete'),

        #Admin role routes:
        \App\Core\Route::get('|^admin/login/?$|',                  'Main',                   'getLogin'),
        \App\Core\Route::post('|^admin/login/?$|',                 'Main',                   'postLogin'),
        \App\Core\Route::get('|^admin-panel/?$|',                  'AdminPanel',             'index'),
        \App\Core\Route::get('|^admin/logout/?$|',                 'Main',                   'getLogout'),
        \App\Core\Route::get('|^admin/products/?$|',               'AdminProductMenagement', 'products'),
        \App\Core\Route::get('|^admin/products/edit/([0-9]+)?$|',  'AdminProductMenagement', 'getEdit'),
        \App\Core\Route::post('|^admin/products/edit/([0-9]+)?$|', 'AdminProductMenagement', 'postEdit'),
        \App\Core\Route::get('|^admin/products/add/?$|',           'AdminProductMenagement', 'getAdd'),
        \App\Core\Route::post('|^admin/products/add/?$|',          'AdminProductMenagement', 'postAdd'),

        \App\Core\Route::any('|^.*$|',                             'Main',                   'home')
    ];