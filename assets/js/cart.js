function getItems() {
    fetch(BASE + 'api/items', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            displayItems(data.items);
        });
}

function addCart(productId) {
	let quantityValue = Number(document.querySelector('#quantity_' + productId).value);
	const url = BASE + 'api/items/add/' + productId + '/' + quantityValue;

    fetch(url, { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                getItems();
            }
        });

    document.getElementById('modal-wrapper').style.display='block';
}

function clearItems() {
    fetch(BASE + 'api/items/clear', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                getItems();
            }
        });
}

function deleteItem(productId) {
    const url = BASE + 'api/items/delete/' + productId;
    fetch(url, { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                getItems();
            }
        });
}

function displayItems(items) {
    const itemsDiv = document.querySelector('.items');
    itemsDiv.innerHTML = '';

    if (items.length === 0) {
        itemsDiv.innerHTML = '<i class="fa fa-opencart cart text-center col-12"></i>';
        return;
    }

    let lista = '';
	let suma = 0;

    for (item of items) {
		suma += Number(item.price) * Number(item.quantity);

        let cartItemView = `
			<div class="cart-item row">
                <div class="cart-item-title col-3"><a href="${BASE}product/${item.product_id}">${item.title}</a></div>
                <div class="cart-item-qty col-2"><p>${item.quantity}&nbsp x &nbsp</p></div>
                <div class="cart-item-price col-2">${item.price}</div>
                <button class="btn btn-danger cart-item-delete col-2" onclick="deleteItem({{ cart_product.product_id }});">X</button>  
            </div>
            <hr>
        `;
        
        lista += cartItemView;
    }

	lista += `<p class="sum">Ukupno: ${suma}</p>`;
	
    itemsDiv.innerHTML = '<div>' + lista + '</div>';
}





addEventListener('load', getItems);