<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class CartProductModel extends Model{
        protected function getFields(): array {
            return [
                'cart_product_id'      => new Field((new NumberValidator())->setIntegerLength(10), false),
                'added_at'             => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                'amount'               => new Field((new NumberValidator())->setIntegerLength(10)),
                'cart_id'              => new Field((new NumberValidator())->setIntegerLength(10)),
                'product_id'           => new Field((new NumberValidator())->setIntegerLength(10))
            ];
        }
    }